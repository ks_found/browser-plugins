var searchPages = [
    '//www.google.',
    '//duckduckgo.com',
    '//www.bing.com',
    '//search.brave.com',
    'search.yahoo.com',
];
var isGoogle = (''+location).includes('//www.google.');
var isSearchPage = false;
for (var i = 0; i < searchPages.length; i++) {
    var se = searchPages[i];
    if ((''+location).includes(se)) {
        isSearchPage = true;
        break;
    }
}
function transform() {
    var links = document.getElementsByTagName('a');
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        if (link.href.includes('en.wikipedia.org/wiki/')) {
            link.href = link.href.replace('en.wikipedia.org/wiki/', 'encycloreader.org/r/wikipedia.php?q=');
            if (isSearchPage) {
                link.innerHTML = 'ER: ' + link.innerHTML;
            }
        }
    }
}
if (!isGoogle) {
    transform();
    var transformAttempts = 1;
    var tId = setInterval(function () {
        transform();
        transformAttempts++;
        if (transformAttempts > 20) {
            clearInterval(tId);
        }
    }, 1000);

}
